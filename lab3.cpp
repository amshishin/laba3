#include <iostream>
#include <list>
using namespace std;

struct Edge {
    int src, dest;
    float weight;
};



class Graph
{
    typedef pair<int, float> Pair;
    list<list<Pair>> adjList;
public:

    Graph()
    {

    }

    Graph(list<Edge> const &edges, int n)
    {

        adjList.resize(n);


        for (auto &edge: edges)
        {
            int src = edge.src;
            int dest = edge.dest;
            float weight = edge.weight;
            auto front = adjList.begin();

            advance(front, src);

            (*front).push_back(make_pair(dest, weight));

        }
    }
    bool empty()
    {
        return this->adjList.size() == 0;
    }

    size_t size()
    {
        return this->adjList.size();
    }

    void clear()
    {
        this->adjList.clear();
    }

    Graph(const Graph & other)
    {
        this->adjList = other.adjList;
    }


    void printGraph()
    {
        int k = 0;
        for (auto i = this->adjList.cbegin(); i != this->adjList.cend(); ++i)
        {
            // Функция для печати всех соседних вершин данной вершины
            for (Pair v: *i) {
                cout << "(" << k << ", " << v.first << ", " << v.second << ") ";
            }
            k++;
            cout << endl;
        }
    }
    auto begin()
    {
        return this->adjList.begin();
    }

    auto cbegin()
    {
        return this->adjList.cbegin();
    }

    auto end()
    {
        return this->adjList.end();
    }

    auto cend()
    {
        return this->adjList.cend();
    }


    void swap(Graph & other) {
        this->adjList.swap(other.adjList);
    }

        size_t degree_out(int key)
    {
        auto front = this->adjList.begin();
        advance(front, key);
        int k = 0;
        for (Pair v: *front) {
            k++;
        }
        return k;
    }
    size_t degree_in(int key)
    {
        int k = 0;
        for (auto i = this->adjList.cbegin(); i != this->adjList.cend(); ++i)
        {
            for (Pair v: *i) {
                if (v.first == key)
                {
                    k++;
                }
            }
        }
        return k;
    }
    bool loop(int key)
    {
        auto front = this->adjList.begin();
        advance(front, key);
        int k = 0;
        for (Pair v: *front)
        {
            if (key == v.first)
            {
                k++;
            }
        }
        return k != 0;
    }

};



int main()
{

    list<Edge> edges =
            {
                    {0, 1, 6}, {1, 2, 7}, {2, 0, 5}, {2, 1, 4}, {3, 2, 10}, {5, 4, 1}, {4, 5, 3}
            };

    int n = 6;

    Graph graph(edges, n);
    Graph graph2;


    graph.printGraph();

    cout << graph.degree_out(2) << endl;
    cout << graph.degree_in(1) << endl;
    cout << graph.loop(1) << endl;
    cout << graph2.size() << endl;
    cout << graph.size() << endl;

    return 0;

}
